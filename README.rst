Identification of new kinematic systems for laser materials processing
===========================================================

* `Supplementary Information <https://thomas-kaster.pages.git-ce.rwth-aachen.de/Identification_of_new_kinematic_systems_for_laser_materials_processing/>`_

Features
--------

* Project is based on the method of the technology intelligence process by `Hicking et al. 2022 <https://link.springer.com/chapter/10.1007/978-3-662-63758-6_13>`_
* Additionally, the technology assessment methodology by `Hicking and Heimes 2022 <https://link.springer.com/chapter/10.1007/978-3-662-63758-6_14>`_ is used
* The special advantages of laser material processing are taken into account in the search and evaluation of kinematic systems 

Acknowledgements
--------

* Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germanys Excellence Strategy – EXC-2023 Internet of Production – 390621612
* The authors acknowledge the financial support by the Federal Ministry of Education and Research of Germany in the framework of Research Campus Digital Photonic Production (project number: 13N15423)

.. image:: resources/IoP_Logo.png
.. image:: resources/Logo_Composing_DPP_BMBF_en.png

Results
-----------------------

Definition of the search fields:
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Mobile Robots, 
* Fixed Robot Arms, 
* Delta Robots, 
* Cartesian Robots, 
* Cable-Driven Robots, 
* Non-Conventional Robots

Result of the search for all search fields:
^^^^^^^^^^^^^^^^^^^^^^^^^^

**Mobile Robots**

.. image:: resources/20230412_LiM2023_SUB_Information_MR_N.svg

`The best evaluated technology in the search field Mobile Robots - RB-KAIROS+ MOBILE MANIPULATOR by Robotnik Automation S.L., © Robotnik Automation S.L., <https://robotnik.eu/products/mobile-manipulators/rb-kairos/>`_ 





**Fixed Robot Arms**

.. image:: resources/20230623_LiM2023_SUB_Information_FRA_N.svg

`The best evaluated technology in the search field Fixed Robot Arms - UR 20 by UNIVERSAL ROBOTS, © Universal Robots 2023 <https://www.universal-robots.com/de/produkte/ur20-roboter/>`_ 

**Delta Robots**

.. image:: resources/20230623_LiM2023_SUB_Information_DR_N.svg

`The best evaluated technology in the search field Delta Robots - DR Delta Robots, © 2023 WEISS GmbH <https://www.weiss-world.com/gb-en/products/robots-10327/delta-robots-211/>`_ 

**Cartesian Robots**

.. image:: resources/20230623_LiM2023_SUB_Information_CR_N.svg

`The best evaluated technology in the search field Cartesian Robots - Bosch Rexroth Kartesische Mehrachssysteme, © Bosch Rexroth AG, <https://www.boschrexroth.com/de/de/produkte/produktgruppen/lineartechnik/themen/mehrachssysteme/>`_ 

**Cable-Driven Robots**

.. image:: resources/20230412_LiM2023_SUB_Information_CDR_N.svg

`The best evaluated technology in the search field Cable-Driven Robots - Principle drawing of the parallel rope robot IPAnema: The end effector is guided and moved over eight ropes, © Fraunhofer IPA, Rainer Bez, <https://www.ipa.fraunhofer.de/de/presse/presseinformationen/2014-03-20_sensorgefuehrt-und-flexibel-montiert.html>`_ 

**Non-Conventional Robots**

.. image:: resources/20230412_LiM2023_SUB_Information_NON_N.svg

`The best evaluated technology in the search field Non-Conventional Robots - MorpHex MKIII, the transforming robot, © Kåre Halvorsen (aka Zenta) 2023 <http://zentasrobots.com/robot-projects/morphex-mkiii/>`_ 


References
-----------------------

ABB (2022) IRB 365 FlexPicker®: 5 axes flexibility for fast reorientation of lightweight products. https://search.abb.com/library/Download.aspx?DocumentID=9AKK108467A1100&LanguageCode=en&DocumentPartId=&Action=Launch. Accessed 15 May 2023

ABB AB - Robotics & Discrete Automation (2015) Product manual IRB 14000. https://search.abb.com/library/Download.aspx?DocumentID=3HAC052983-001&LanguageCode=en&DocumentPartId=&Action=Launch. Accessed 15 May 2023

ABI B.V. (2014) ABIflexx Wide Delta Robot Specifications. https://www.abi.nl/en/products-and-services/products/robotics/abiflexx-robotic-systems/abiflexx-wide-delta-robot-specifications/. Accessed 15 May 2023

Aerowash AB (2017) Aerowash: Products and Services. https://www.aerowash.com/products-and-services/. Accessed 15 May 2023

Bosch Rexroth AG (2022) Cartesian multi-axis systems for positioning and handling tasks. https://www.boschrexroth.com/en/pl/products/product-groups/linear-motion-technology/topics/cartesian-multi-axis-systems/. Accessed 8 May 2023

Bostelman R, Albus J, Dagalakis N, Jacoff A (1996) RoboCrane Project: An Advanced Concept for Large Scale Manufacturing. In: Proceedings of the AUVSI Conference

Boston Dynamics (2022) Stretch TM: Mobile, automated case handling for more efficient warehouse operations. https://www.bostondynamics.com/products/stretch. Accessed 8 May 2023

Codian Robotics BV (2022) D5-1600-S050G-R300. https://www.codian-robotics.com/producten/d5-1600-s050g-r300/. Accessed 15 May 2023

COMAU (2016) Rebel-S6-0.75. https://www.comau.com/en/competencies/robotics-automation/robot-team/rebel-s6-0-75/

Comau S.p.A. (2011) NJ4-165-3.4 SH Flexible, Space-Saving Automation: The Shelf-Mounted Hollow Wrist Robot. https://www.comau.com/en/competencies/robotics-automation/robot-team/nj4-165-3-4-sh/. Accessed 15 May 2023

DC Velocity Magazine (2017) HIROTEC AMERICA Partners with OTTO Motors for Lights-Out Manufacturing with Mobile Manipulation. https://www.dcvelocity.com/articles/34365-hirotec-america-partners-with-otto-motors-for-lights-out-manufacturing-with-mobile-manipulation/. Accessed 8 May 2023

DFKI GmbH - Robotics Innovation Center (2018) Full Body Exoskeleton: Exoskeleton for upper body robotic assistance. https://robotik.dfki-bremen.de/uploads/tx_dfkiprojects/20180913_Systemblatt_recupera_Ganzkoerper_de.pdf. Accessed 15 May 2023

DLR Institute of Robotics, Mechatronics (2018) CAESAR (Compliant Assistance and Exploration SpAce Robot). https://www.dlr.de/rm/en/desktopdefault.aspx/tabid-13282/#gallery/32051. Accessed 15 May 2023

ENGEL AUSTRIA GmbH (2022) Linear Robot viper: Powerful and Flexible to Use. https://www.engelglobal.com/en/products/injection-moulding-automation/linear-robot. Accessed 15 May 2023

FANUC America Corporation (2020) FANUC R-2000iC/100P. https://www.fanucamerica.com/products/robots/series/r-2000/r-2000ic-100p. Accessed 15 May 2023

FANUC Deutschland GmbH (2019) M-3iA/6A. https://www.fanuc.eu/~/media/files/pdf/products/robots/robots-datasheets-en/m-3ia/datasheet%20m-3ia-6a.pdf?la=en. Accessed 15 May 2023

Festo AG & Co. KG (2019) BionicSoftArm: Modular pneumatic lightweight robot. https://www.festo.com/net/SupportPortal/Files/597075/Festo_BionicSoftArm_en.pdf

FESTO Corporation (2021) Cantilever system YXCA. https://www.festo.com/media/pim/278/D15000100173278.PDF. Accessed 15 May 2023

FESTO Corporation (2022) Three-dimensional gantry YXCR. https://www.festo.com/media/pim/293/D15000100173293.PDF. Accessed 15 May 2023

Fibro Laepple Technology Inc. (2022) AGR: Area Gantry Robot. https://flt-us.com/products/modular-axis-and-gantry-system/area-gantry-robot/. Accessed 15 May 2023

Fraunhofer IFAM (2016) CNC Machining Robot: Pressemitteilung. https://www.ifam.fraunhofer.de/content/dam/ifam/en/documents/Adhesive_Bonding_Surfaces/FFM/CNC_Machining_Robot_fraunhofer_ifam.pdf. Accessed 8 May 2023

Fraunhofer IPA (2014) IPAnema – Seilroboter für die Intralogistik: Pressemitteilung. https://www.ipa.fraunhofer.de/de/presse/presseinformationen/2014-03-20_sensorgefuehrt-und-flexibel-montiert.html. Accessed 8 May 2023

Girin A, Dayez-Burgeon P (2021) FASTKIT – Collaborative and mobile cable driven parallel robot for logistics. https://echord.eu/fastkit/index.php.html. Accessed 15 May 2023

Güdel Group AG (2022) FP-5-HP: Beste Kombination aus grösstmöglichem Hub, hoher Dynamik und Nutzlast. https://media.gudel.com/v/5ZVFJdty/. Accessed 15 May 2023

Halvorsen K (2015) MorpHex MKIII. http://zentasrobots.com/robot-projects/morphex-mkiii/. Accessed 8 May 2023

Hicking J, Heimes P (2022) Technologiebewertung: Technologiebewertung aus wirtschaftlicher, technischer und praktischer Perspektive. In: Schuh G, Zeller V, Stich V (eds) Digitalisierungs- und Informationsmanagement: Handbuch Produktion und Management 9, 1st edn. Springer Berlin Heidelberg, Berlin, Heidelberg, pp 347–371

Hicking J, Stroh M. (2022) Technologiefrüherkennung. In: Schuh G, Zeller V, Stich V (eds) Digitalisierungs- und Informationsmanagement: Handbuch Produktion und Management 9, 1st edn. Springer Berlin Heidelberg, Berlin, Heidelberg, pp 329–346

IAAC Barcelona (2022) MINIBUILDERS: SMALL ROBOTS PRINTING LARGE-SCALE STRUCTURES. https://iaac.net/project/minibuilders/. Accessed 8 May 2023

IAI Industrieroboter GmbH (2022) Matching cartesian robots 6-axis combinations. https://www.iai-automation.com/files/content/downloads/brochures/ICSB-ICSA_Part3_3-Axis-Gantry-4-6-Axis-System_eng.pdf#page=93. Accessed 15 May 2023

Industrial Robotics M, Automation Laboratory (2022) Cable-Driven Robots. https://irmalab.org/research/cable-driven-robots/. Accessed 15 May 2023

Jeffrey C (2014) Multi-delta design enables lighter, more economical robotic arm. https://newatlas.com/delta-robot-lighter-economical-robotic-arm/34343/

Khajepour A (2022) Cable-driven parallel robots. https://uwaterloo.ca/mechatronic-vehicle-systems-lab/research/cable-driven-parallel-robots. Accessed 15 May 2023

Kooistra L (2014) Unmanned Aerial Vehicle for Laser Scanning (LiDAR UAV). https://www.wur.nl/en/product/Unmanned-Aerial-Vehicle-for-Laser-Scanning-LiDAR-UAV.htm. Accessed 15 May 2023

KUKA AG (2013) LBR iiwa: A feel for the production world of tomorrow. https://www.kuka.com/-/media/kuka-downloads/imported/87f2706ce77c4318877932fb36f6002d/kuka_lbr_iiwa_brochure_en.pdf?rev=dbb717cb1d3d4b2797849d7a0429045c&hash=C6EB201138F8631C0F36EB7099461B28. Accessed 15 May 2023

KUKA AG (2021) KR DELTA. https://www.kuka.com/en-de/products/robot-systems/industrial-robots/kr-delta-roboter. Accessed 15 May 2023

KUKA AG (2022a) flexFELLOW: KUKA flexFELLOW: the flexible automation concept. https://www.kuka.com/en-de/products/mobility/mobile-robots/kuka-flexfellow. Accessed 8 May 2023

KUKA AG (2022b) KMR QUANTEC: KMR QUANTEC: autonomous and precise. https://www.kuka.com/en-de/products/mobility/mobile-robots/kmr-quantec. Accessed 8 May 2023

KUKA AG (2022c) KR 600 FORTEC: Absolute precision for a wide range of applications. https://www.kuka.com/-/media/kuka-downloads/imported/87f2706ce77c4318877932fb36f6002d/kr-fortec-en.pdf?rev=134c33fc78ca41bba93d694a6c22a075&hash=D4A929B8B6F9A0916D857A7880A02B26. Accessed 15 May 2023

KUKA AG (2022d) KUKA linear robots. https://www.kuka.com/en-de/products/robot-systems/industrial-robots/linear-robots. Accessed 15 May 2023

Liebherr-International AG (2022) LPR 20. https://www.liebherr.com/de/int/produkte/verzahntechnik-automation/automationssysteme/portalroboter/fl%C3%A4chenportalroboter/details/lpr20.html. Accessed 15 May 2023

Max-Planck-Gesellschaft zur Förderung der Wissenschaften e.V. (2016) The CableRobot Simulator. https://www.cyberneum.de/CableRobotSimulator. Accessed 15 May 2023

Mobile Industrial Robots A/S (2022) MiR Hook 250. https://www.mobile-industrial-robots.com/solutions/mir-accessories/mir-hook-250/. Accessed 8 May 2023

Modes H, Karguth A (2022) Aerobotics: reaching new heights. http://www.aerobotics.de/index-eng.html. Accessed 15 May 2023

Nordic Dino Robotics AB (2021) Aircraft washing solutions. https://www.nordicdino.com/. Accessed 15 May 2023

Paijens AFM (2021) Localization of Mobile Robots for Precision Manufacturing, Auckland University of Technology

Penta Robotics (2018) Veloce. https://pentarobotics.com/products//. Accessed 15 May 2023

Piao J, Jin X, Jung J, Choi E, Park J-O, Kim C-S (2017) Development of a high payload cable-driven parallel robot. In: 2017 17th International Conference on Control, Automation and Systems (ICCAS), pp 423–425

Robotnik Automation S.L. (2022) RB-KAIROS+ MOBILE MANIPULATOR: RB-KAIROS+ is presented as a solution for Universal Robots e-Series users. https://robotnik.eu/products/mobile-manipulators/rb-kairos/. Accessed 8 May 2023

Shaper Tools GmbH (2020) Origin Handheld Precision Routing. https://www.shapertools.com/en-de/origin. Accessed 15 May 2023

Sousa JP, Palop C, Moreira E, Pinto A, Lima J, Costa P, Costa P, Veiga G, Moreira A (2016) The SPIDERobot: A Cable-Robot System for On-site Construction in Architecture. In: Dagmar Reinhardt, Rob Saunders, Jane Burry (ed) Robotic Fabrication in Architecture, Art and Design 2016, pp 230–239

spidercam GmbH – Germany (2010) Custom Cable Robotics Solutions. https://www.spidercam.tv/custom-cable-robotics-solutions/. Accessed 15 May 2023

Stäubli International AG (2022) Mobiles Robotersystem HelMo. https://www.staubli.com/global/de/robotics/produkte/mobiles-robotersystem.html. Accessed 8 May 2023

Stormbee / JP Engineering BV (2022) Aerial Mapping System STORMBEE. https://stormbee.com/drone-mapping/. Accessed 15 May 2023

Tormach (2022) Tormach ZA6 INDUSTRIAL ROBOT. https://tormach.com/machines/robots.html. Accessed 15 May 2023

Tractus 3D (2022) T3500SE. https://tractus3d.com/de/products/t3500/. Accessed 15 May 2023

Universal Robots A/S (2022) INTRODUCING THE UR20: This is the cobot. Redefined. https://www.universal-robots.com/products/ur20-robot/. Accessed 15 May 2023

WEISS UK Ltd. (2021) DR Delta Robots: Breathtaking speed. https://www.weiss-world.com/gb-en/products/robots-10327/delta-robots-211. Accessed 15 May 2023

Yamaha Motor (2022) Cantilever system YXCA. https://global.yamaha-motor.com/business/robot/lineup/xyx/shared/pdf/a_sxybx_2-c.pdf. Accessed 15 May 2023

YAMAHA Motor Co. (2012) Orbit type SCARA robots YK-TW. https://global.yamaha-motor.com/business/robot/lineup/ykxg/orbit/. Accessed 15 May 2023

Yaskawa Motoman (2015) MPP3H High-Speed Delta Robot. https://www.yaskawa.com/products/robotics/robots-with-iec/delta-robots/mpp3h. Accessed 15 May 2023

Yaskawa Motoman (2022) HC20DTP: Easy-to-Clean Collaborative Robot with Hand-Guided Teaching. https://www.motoman.com/en-us/products/robots/collaborative/hc-series/hc20dtp. Accessed 15 May 2023


