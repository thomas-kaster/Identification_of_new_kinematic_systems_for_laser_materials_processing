=======
Credits
=======

Development Lead
----------------

* Thomas Kaster <thomas.kaster@llt.rwth-aachen.de>

Contributors
------------

* Felipe Arango Callejas <Felipe.arango.callejas@llt.rwth-aachen.de>
* Leon Gorißen <leon.gorissen@llt.rwth-aachen.de>
* Jan-Niklas Schneider <jan-niklas.schneider@llt.rwth-aachen.de>
* Philipp Walderich <philipp.walderich@llt.rwth-aachen.de>
* Christian Hinke <christian.hinke@llt.rwth-aachen.de>
