.. include:: ../README.rst 
  
Contents
--------
.. toctree::
   :maxdepth: 2

   data
   authors

Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


